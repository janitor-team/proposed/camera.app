
// created 11.2003 by Stefan Kleine Stegemann
// 
// licensed under GPL


#include "OpenPanelAddons.h"


/*
 * Accessory view for an open panel to let the user
 * select whether the downloaded files should be
 * placed in a timestamped directory.
 */
@implementation OpenPanelAccessoryView

- (id) init
{
   if ((self = [super initWithFrame: NSMakeRect(0, 0, 0, 0)]))
   {
      useTimestampDirectory = [[NSButton alloc] initWithFrame: [self frame]];
      [useTimestampDirectory setTitle: @"place files in timestamped directory"];
      [useTimestampDirectory setButtonType: NSSwitchButton];

      [self addSubview: AUTORELEASE(useTimestampDirectory)];
      [useTimestampDirectory sizeToFit];
      [self setFrame: [useTimestampDirectory frame]];
   }
   return self;
}


/*
 * Factory method for convinience. The width of the accessory
 * view is adjusted to the width of the specified panel.
 */
+ (OpenPanelAccessoryView*) accessoryView
{
   id view = [[OpenPanelAccessoryView alloc] init];
   return AUTORELEASE(view);
}


- (void) setUseTimestampeDirectory: (BOOL)use
{
   [useTimestampDirectory setState: (use ? NSOnState : NSOffState)];
}


- (BOOL) useTimestampDirectory
{
   return ([useTimestampDirectory state] == NSOnState);
}

@end
