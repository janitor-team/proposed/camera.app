
// created 11.2003 by Stefan Kleine Stegemann
// 
// licensed under GPL

#ifndef _H_OPEN_PANEL_ADDONS
#define _H_OPEN_PANEL_ADDONS

#include <AppKit/NSView.h>
#include <AppKit/NSButton.h>
#include <AppKit/NSPanel.h>


@interface OpenPanelAccessoryView : NSView
{
   NSButton*  useTimestampDirectory;
}


- (id) init;
+ (OpenPanelAccessoryView*) accessoryView;

- (void) setUseTimestampeDirectory: (BOOL)use;
- (BOOL) useTimestampDirectory;

@end


#endif
