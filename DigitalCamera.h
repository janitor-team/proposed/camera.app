
// created 11.2003 by Stefan Kleine Stegemann
// 
// licensed under GPL

#ifndef _H_DIGITAL_CAMERA
#define _H_DIGITAL_CAMERA

#include <Foundation/NSObject.h>
#include <Foundation/NSString.h>
#include <Foundation/NSArray.h>
#include <AppKit/NSImage.h>

@class DigitalCameraFile;

@interface DigitalCamera : NSObject
{
   NSString* name;
   NSString* portName;

   // libgphoto stuff
   void* gpCamera;
   void* gpContext;
}

- (id) initWithName: (NSString*)_name
             atPort: (NSString*)_portName
           gpCamera: (void*)_gpCamera
          gpContext: (void*)_gpContext;
- (void) dealloc;

- (NSString*) name;
- (NSString*) portName;

- (NSArray*) availableFiles;
- (NSImage*) thumbnailForFile: (DigitalCameraFile*)file;
- (void) downloadFile: (DigitalCameraFile*)file to: (NSString*)destination;
- (void) deleteFile: (DigitalCameraFile*)file;

+ (NSArray*) autodetectCameras;

@end



@interface DigitalCameraFile : NSObject
{
   NSString* filename;
   NSString* folder;
}

- (id) initWithFilename: (NSString*)_filename
               inFolder: (NSString*)_folder
               onCamera: (DigitalCamera*)camera;
- (void) dealloc;
- (NSString*) filename;
- (NSString*) folder;

@end

#endif
